## Nota
Primeiramente quero pedir desculpas porque não consegui implementar muitas coisas que deveria e gostaria, 
como importação e exportação de .csv, uma boa validação dos endpoints, criação de testes com o `pytest`, OAuth 2.0, 
uma melhor arquitetura, etc. E gostaria também de agradecer a oportunidade, nunca trabalhei com Flask e graças a ele 
não ser um micro framework opinado me ajudou a enchergar vários pontos básicos que preciso melhorar.  
Aliás, caso queira me avaliar também pelos meus projetos públicos no GitHub, [esse é o link](https://github.com/bok-jin).

## Requisitos do servidor
- Python >= 3.7
- Postgres >= 11

## Instalação e configuração
    # clone o repositório
    $ git clone https://bok-jin@bitbucket.org/bok-jin/api.git
    # crie um ambiente virtual e ative-o
    $ python3 -m venv venv
    $ . venv/bin/activate
    # instale as dependências
    $ pipenv install
    # altere o que for preciso em .flaskenv e app/config.py

## Execução
    $ flask run

## Recursos disponíveis na API exemplificados em Python
Insere um usuário.
```python
import http.client

conn = http.client.HTTPConnection("127.0.0.1:5000")

payload = "{\n\t\"name\": \"Jin Bok\",\n\t\"address\": \"Osasco, Brazil\",\n\t\"phone\": \"(11) 96703-0375\"\n}"

headers = { 'content-type': "application/json" }

conn.request("POST", "/users", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

Recupera todos os usuários, com paginação.
```python
import http.client

conn = http.client.HTTPConnection("127.0.0.1:5000")

payload = ""

conn.request("GET", "/users/page/1", payload)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

Recupera um usuário, especificado pelo seu ID.
```python
import http.client

conn = http.client.HTTPConnection("127.0.0.1:5000")

payload = ""

conn.request("GET", "/users/1", payload)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

Atualiza dados de um usuário, especificado pelo seu ID.
```python
import http.client

conn = http.client.HTTPConnection("127.0.0.1:5000")

payload = "{\n\t\"name\": \"Jin Jin Jin\"\n}"

headers = { 'content-type': "application/json" }

conn.request("PUT", "/users/1", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

Deleta permanentemente um usuário, especificado pelo seu ID.
```python
import http.client

conn = http.client.HTTPConnection("127.0.0.1:5000")

payload = ""

conn.request("DELETE", "/users/1", payload)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

Feito com ❤  por Jin Bok
