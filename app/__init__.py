from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from app import config

app = Flask(__name__)

app.config.from_object(config.Development)

db = SQLAlchemy(app)

from .models import Users

# migrate models
db.create_all()


from .api import api
app.register_blueprint(api)


if __name__ == '__main__':
    app.run()
