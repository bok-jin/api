from flask import Blueprint, request, jsonify
from app import db, Users

api = Blueprint('api', __name__)


@api.route('/users/page/<int:page>')
def index(page):
    users = Users.query.paginate(page, error_out=True, max_per_page=10)

    response = []

    for user in users.items:
        data = {
            'id': user.id,
            'name': user.name,
            'address': user.address,
            'phone': user.phone,
            'active': user.active,
            'created_at': user.created_at
        }

        response.append(data)

    return jsonify({
        'users': response,
        'total_users': users.total,
        'total_pages': users.pages
    })


@api.route('/users/<int:id>')
def show(id):
    user = Users.query.get(id)

    response = {
        'id': user.id,
        'name': user.name,
        'address': user.address,
        'phone': user.phone,
        'active': user.active,
        'created_at': user.created_at
    }

    return jsonify({'user': response})


@api.route('/users', methods=['POST'])
def store():
    data = request.get_json()

    new_user = Users(name=data['name'], address=data['address'], phone=data['phone'])

    db.session.add(new_user)
    db.session.commit()

    return jsonify({'message': 'User successfully created'})


@api.route('/users/<int:id>', methods=['PUT'])
def update(id):
    data = request.get_json()

    user = Users.query.get(id)

    if 'name' in data:
        user.name = data['name']
    elif 'address' in data:
        user.address = data['address']
    elif 'phone' in data:
        user.phone = data['phone']
    elif 'active' in data:
        user.active = data['active']

    db.session.commit()

    return jsonify({'message': 'User successfully updated'})


@api.route('/users/<int:id>', methods=['DELETE'])
def destroy(id):
    user = Users.query.get(id)

    db.session.delete(user)
    db.session.commit()

    return jsonify({'message': 'User successfully deleted'})
